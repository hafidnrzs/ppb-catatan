import 'package:catatan/catatan.dart';
import 'package:catatan/styles.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CatatanPage(),
    );
  }
}

class CatatanPage extends StatefulWidget {
  const CatatanPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => CatatanState();
}

TextEditingController judulCtrl = TextEditingController();
TextEditingController catatanCtrl = TextEditingController();

class CatatanState extends State<CatatanPage> {
  List<Catatan> listCatatan = Catatan.dataDummy;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Catatan Pagi"),
        backgroundColor: const Color(0xFFDDA15E),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: TextField(
              controller: judulCtrl,
              decoration: const InputDecoration(hintText: "Judul.."),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: TextField(
              controller: catatanCtrl,
              decoration: const InputDecoration(hintText: "Catatan.."),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        clearForm();
                      });
                    },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: buttonBgColor),
                    child: const Text("Clear", style: buttonTextStyle),
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                    child: ElevatedButton(
                  onPressed: () {
                    if (judulCtrl.text != '' && catatanCtrl.text != '') {
                      setState(() {
                        listCatatan
                            .add(Catatan(judulCtrl.text, catatanCtrl.text));
                        clearForm();
                      });
                    }
                  },
                  style:
                      ElevatedButton.styleFrom(backgroundColor: buttonBgColor),
                  child: const Text("Submit", style: buttonTextStyle),
                )),
              ],
            ),
          ),
          Expanded(
              child: ListView.builder(
            itemCount: listCatatan.length,
            itemBuilder: (context, index) {
              return Container(
                color: const Color(0XFFFEFAE0),
                padding: const EdgeInsets.all(12.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(listCatatan[index].judul,
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      const SizedBox(height: 8.0),
                      Text(listCatatan[index].isi,
                          style: const TextStyle(
                            fontSize: 16,
                          )),
                      const SizedBox(height: 8.0)
                    ]),
              );
            },
          ))
        ],
      ),
    );
  }

  void clearForm() {
    judulCtrl.text = '';
    catatanCtrl.text = '';
  }
}
