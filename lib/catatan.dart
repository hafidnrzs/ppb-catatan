class Catatan {
  String judul;
  String isi;

  Catatan(
    this.judul,
    this.isi,
  );

  static List<Catatan> dataDummy = [
    Catatan(
      "ListView builder",
      "ListView is a very important widget in a flutter. It is used to create the list of children But when we want to create a list recursively without writing code again and again then ListView.builder is used instead of ListView.",
    ),
    Catatan(
      "Catatan Hari Ini",
      "Hari ini latihan menjelang UTS, membuat aplikasi catatan memanfaatkan TextField, ListView builder, dan StatefulWidget.",
    ),
  ];
}
